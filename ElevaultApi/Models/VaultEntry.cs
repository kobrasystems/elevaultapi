﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaultApi.Models
{
    public class VaultEntry
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("events")]
        public string[] Events { get; set; }
        [BsonElement("comments")]
        public string[] Comments { get; set; }
        [BsonElement("mood")]
        public string Mood { get; set; }
        [BsonElement("dayRating")]
        public int DayRating { get; set; }
        [BsonElement("userId")]
        public string UserId { get; set; }
        [BsonElement("date")]
        public DateTime Date { get; set; }

    }
}
