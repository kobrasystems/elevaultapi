﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevaultApi.Services
{
    public class AuthService
    {
        public static TokenValidationParameters GetTokenValidationParameters(string jwtKey)
        {
            var jwtKeyBytes = Encoding.UTF8.GetBytes(jwtKey);

            return new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(jwtKeyBytes),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero

            };
        }

    }
}
